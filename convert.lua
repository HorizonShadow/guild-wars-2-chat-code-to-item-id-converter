require 'base64'
local input
local sub = string.sub
local pow = math.pow

local hex_index = {
	['0'] = 0,
	['1'] = 1,
	['2'] = 2,
	['3'] = 3,
	['4'] = 4,
	['5'] = 5,
	['6'] = 6,
	['7'] = 7,
	['8'] = 8,
	['9'] = 9,
	['a'] = 10,
	['b'] = 11,
	['c'] = 12,
	['d'] = 13,
	['e'] = 14,
	['f'] = 15,
}

function promptForInput()
	io.write("Input a chat link, or type quit.\n")
	io.flush()
end

function getUserInput()
	return io.read()
end

function isValidCode(s)
	return sub(s,1,1) == '[' and sub(s,2,2) == '&' and sub(s, -1) == ']'
end

function isQuitCommand(s)
	return s == 'quit'
end

function displayError()
	io.write("Not a valid chat code.\n")
	io.flush()
end

function convertCodeToHex(s)
	local properCode = string.sub(s, 3,-2)
	local hex = base64ToBase16(properCode)
	return hex
end

function flipOrder(s)
	local temp = sub(s, 3)
	local temp2 = sub(s, 0,2)
	local id = temp .. temp2
	return id
end
function getId(s)
	local id = sub(s, 5, 8)
	id = flipOrder(id)
	local total = 0
	local p = 3
	for i = 1, #id do
		local n = tonumber(hex_index[sub(id,i,i)])
		total = total + n * (pow(16,p))
		p = p - 1
	end
	return total
end
repeat
	local validCode, hex, id
	promptForInput()
	input = getUserInput()
	if isValidCode(input) then
		hex = convertCodeToHex(input)
		id = getId(hex)
		io.write(id, '\n')
	else
		if not isQuitCommand(input) then
			displayError()
		end
	end
until isQuitCommand(input)

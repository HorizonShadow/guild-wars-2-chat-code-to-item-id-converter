local hex_index = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'}
local index_table = {
		A = '000000', --0
		B = '000001', --1
		C = '000010', --2
		D = '000011', --3
		E = '000100', --4
		F = '000101', --5
		G = '000110', --6
		H = '000111', --7
		I = '001000' , --8
		J = '001001', --9
		K = '001010', --10
		L = '001011', --11
		M = '001100', --12
		N = '001101', --13
		O = '001110', --14
		P = '001111', --15
		Q = '010000', --16
		R = '010001', --17
		S = '010010', --18
		T = '010011', --19
		U = '010100', --20
		V = '010101', --21
		W = '010110', --22
		X = '010111', --23
		Y = '011000', --24
		Z = '011001', --25
		a = '011010', --26
		b = '011011', --27
		c = '011100', --28
		d = '011101', --29
		e = '011110', --30
		f = '011111', --31
		g = '100000', --32
		h = '100001', --33
		i = '100010', --34
		j = '100011', --35
		k = '100100', --36
		l = '100101', --37
		m = '100110', --38
		n = '100111', --39
		o = '101000', --40
		p = '101001', --41
		q = '101010', --42
		r = '101011', --43
		s = '101100', --44
		t = '101101', --45
		u = '101110', --46
		v = '101111', --47
		w = '110000', --48
		x = '110001', --49
		y = '110010', --50
		z = '110011', --51
		['0'] = '110100', --52
		['1'] = '110101', --53
		['2'] = '110110', --54
		['3'] = '110111', --55
		['4'] = '111000', --56
		['5'] = '111001', --57
		['6'] = '111010', --58
		['7'] = '111011', --59
		['8'] = '111100', --60
		['9'] = '111101', --61
		['+'] = '111110', --62
		['/'] = '111111' --63
	}
local function removeExtraZeroes(s)
	local numExtra = #s % 4
	return s:sub(numExtra + 1)
end
local function base64ToBase2(s)
	local binaryString = ''
	for i = 1, #s do
		local c = s:sub(i,i)
		binaryString = binaryString .. index_table[c]
	end
	binaryString = removeExtraZeroes(binaryString)
	return binaryString
end
function base64ToBase16(s)
	local binaryString = base64ToBase2(s)
	local hexString = ''
	local total = 0
	local currCount = 8
	for i = 1, #binaryString do
		local c = binaryString:sub(i,i)
		local n = tonumber(c)
		total = total + n * currCount 
		currCount = currCount / 2
		if i % 4 == 0 then
			hexString = hexString .. hex_index[total+1]
			currCount = 8
			total = 0
		end
	end
	return hexString
end
